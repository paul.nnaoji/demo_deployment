// Home.js
import { Link } from 'react-router-dom';

const About = () => {
  return (
    <div>
      <h1>About Us</h1>
      <nav>
        <ul>
          <li>
            <Link to="/">Users</Link>
          </li>
        </ul>
      </nav>
      Welcome to this demo app
    </div>
  );
};

export default About;